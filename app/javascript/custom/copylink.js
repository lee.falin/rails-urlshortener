document.addEventListener("turbo:load", () => {
	
	let copyButtons = document.querySelectorAll(".copy-button");
	if(!copyButtons)
		return;

	copyButtons.forEach( b => {
		b.addEventListener("click", e => {
			e.preventDefault();
			let clipboardText = b.getAttribute("data-clipboard-text");
			navigator.clipboard.writeText(clipboardText);

			b.innerHTML = "Copied! <i class='fa-duotone fa-clipboard-check'></i>";

			setTimeout( () => {
				b.innerHTML = "Copy <i class='fa-duotone fa-clipboard'></i>";
			}, 1000)
		})
	}) 

})