class Link < ApplicationRecord
	
	# Initial defaults
	before_create do 
		self.count = 0
		self.short_url = Link.random_hash
	end

	# Use a custom validator to make sure this looks like a URL
	validates :url, presence: true, url: true

	# Use a 6-character hash encoded as Base-36.
	def Link.HASH_SEED_LENGTH
		6
	end

	def Link.random_hash
		the_hash = nil

		# It's unlikely we'd get a hash collision in the context of this demo app,
		# but just to make sure...
		while the_hash.nil? || Link.find_by(short_url: the_hash)
			the_hash = SecureRandom.random_number(36**Link.HASH_SEED_LENGTH).to_s(36)	
		end

		return the_hash
	end

end
