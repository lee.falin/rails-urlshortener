class LinksController < ApplicationController
	
	def new
		@link = Link.new
	end

	def index
		@links = Link.all.order(created_at: :desc)
	end

	def create
		@link = Link.new(link_params)
		if @link.save
			flash[:success] = (render_to_string "_success_message", layout: false).html_safe
			redirect_to root_path
		else
			render 'new', status: :unprocessable_entity
		end
	end

	def destroy
		@link = Link.find(params[:id])
		@link.destroy
		redirect_to root_path
	end

	# This is the main logic of the redirect service. First, try to fetch the short url from the DB.
	# If it exists, redirect to the original link and increment the click counter.
	def redirect
		@link = Link.find_by(short_url: params[:short_url])
		if @link
			@link.count += 1
			@link.save
			redirect_to @link.url, allow_other_host: true
		else
			redirect_to root_path
		end
	end

	private
		def link_params
			params.require(:link).permit(:url)
		end

end
