module LinksHelper
	def absolute_short_url(link)
		port = request.port == 80 || request.port == 443 ? "" : ":#{request.port}"
		"#{request.scheme}://#{request.host}#{port}/#{link.short_url}"
	end
end
