module ApplicationHelper
	def page_title(subtitle='')
		base_title = "Link-O-Matic"
		if subtitle.empty?
			base_title
		else
			"#{base_title} | #{subtitle}"
		end
	end

	def flash_sanitize(message)
		sanitize message, attributes: ["target", "href", "class", "data-clipboard-text"], 
								tags: ["div", "p", "a", "button", "i"] 
	end
end
