class UrlValidator < ActiveModel::EachValidator
	
	# There isn't a super great way to validate URLs, even with complex regular expressions
	# so this is a quick version that does a mediocre job at it
	def validate_each(record, attribute, value)
		uri = URI.parse(value)
		unless uri && !uri.scheme.nil? && !uri.host.nil? && uri.kind_of?(URI::HTTP)
			record.errors.add attribute, (options[:message] || "is not a valid url. Make sure the URL is valid and begins with http:// or https://")
		end
	end

end