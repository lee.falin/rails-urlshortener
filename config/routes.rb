Rails.application.routes.draw do
	
	root	"links#index"
	resources :links
	
	get		"/about",				to:"static_pages#about"
	get		"/:short_url",	to:"links#redirect"

end
