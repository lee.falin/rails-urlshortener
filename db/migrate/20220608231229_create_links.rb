class CreateLinks < ActiveRecord::Migration[7.0]
  def change
    create_table :links do |t|
      t.string :url
      t.string :short_url
      t.integer :count

      t.timestamps
    end
  end
end
