class AddIndexToLinksShorturl < ActiveRecord::Migration[7.0]
  def change
    add_index :links, :short_url, unique: true
  end
end
