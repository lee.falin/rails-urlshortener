# README

Link-O-Matic is a URL-shortening service that tracks how often generated links have been clicked.

It's [hosted on Heroku](https://railsdemo.leefalin.com) and was built using the following:

* Ruby on Rails
* Vanilla JavaScript
* The Bulma CSS Framework
* FontAwesome 6
* Postgres

## How It Works

When you enter a URL into the new link form, the app generates a six character hash in [Base36](https://en.wikipedia.org/wiki/Base36). 

![Screen Shot 2022-06-09 at 6 06 30 PM](https://user-images.githubusercontent.com/1585316/172964975-632cf63c-042e-4520-99ca-a5fdb9a76183.png)

Example:

     https://SomeLongURL.com?param1=a&param2=b -> <app host name>/fs3fv1

Obviously just how short the final URL becomes depends a great deal on the length of the host name.

![Screen Shot 2022-06-09 at 6 06 14 PM](https://user-images.githubusercontent.com/1585316/172965008-f5d1c4c0-7ded-44e5-afcc-f1971124a11c.png)

## Notes

The app also includes a handful of unit tests, helper functions, and some JavaScript code to handle the link copying.

The app also has responsive styling for mobile devices, switching from a table to a card view, depending on the screen size.

![Screen Shot 2022-06-09 at 6 07 34 PM](https://user-images.githubusercontent.com/1585316/172965114-7a04abc4-19c4-4e56-bea6-d60b8cd1cfe9.png)
