require "test_helper"

class LinksControllerTest < ActionDispatch::IntegrationTest

  def setup
    @link = Link.new(url: "https://google.com", short_url:nil, count:nil)
    @link.save
  end

  test "should redirect" do
    short_url = @link.short_url
    get "/#{short_url}"
    assert_redirected_to @link.url
  end

  test "count should increment after redirect" do
    current_count = @link.count
    short_url = @link.short_url
    get "/#{short_url}"
    assert_equal current_count + 1, @link.reload.count
  end

end
