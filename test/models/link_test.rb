require "test_helper"

class LinkTest < ActiveSupport::TestCase
  def setup
    @link = Link.new(url: "https://example.com", short_url:nil, count:nil)
  end

  test "should save" do
    assert @link.save
  end

  test "should have hash after save" do
     @link.save
     assert_equal Link.HASH_SEED_LENGTH, @link.short_url.length
  end

  test "should have zero count on save" do
     @link.save
     assert_equal 0, @link.count
  end

  test "count should persist between saves" do
     @link.save
     @link.count = 42
     @link.save
     @link.reload
     assert_equal 42, @link.count
  end

  test "shold accept valid urls" do
    good_link = Link.new(url:"https://example.com", short_url:nil, count:nil)
    assert good_link.valid?
  end

  test "should reject invalid urls" do
    bad_link = Link.new(url:"", short_url:nil, count:nil)
    assert_not bad_link.valid?

    bad_link = Link.new(url:"not-a-url", short_url:nil, count:nil)
    assert_not bad_link.valid?
  end

end
